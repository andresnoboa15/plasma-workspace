# Translation of kcm_autostart.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2008, 2009, 2010, 2011, 2012, 2015, 2017.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-26 02:17+0000\n"
"PO-Revision-Date: 2017-09-28 17:58+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: autostartmodel.cpp:320
#, fuzzy, kde-format
#| msgid "\"%1\" is not an absolute path."
msgid "\"%1\" is not an absolute url."
msgstr "‘%1’ nije apsolutna putanja."

#: autostartmodel.cpp:323
#, kde-format
msgid "\"%1\" does not exist."
msgstr "‘%1’ ne postoji."

#: autostartmodel.cpp:326
#, kde-format
msgid "\"%1\" is not a file."
msgstr "‘%1’ nije fajl."

# >> %1 file path
#: autostartmodel.cpp:329
#, kde-format
msgid "\"%1\" is not readable."
msgstr "‘%1’ nije čitljiv."

#: package/contents/ui/main.qml:41
#, kde-format
msgid "Make Executable"
msgstr ""

#: package/contents/ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: package/contents/ui/main.qml:57
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: package/contents/ui/main.qml:95
#, fuzzy, kde-format
#| msgid "&Properties..."
msgid "Properties"
msgstr "&Svojstva..."

#: package/contents/ui/main.qml:101
#, fuzzy, kde-format
#| msgid "&Remove"
msgid "Remove"
msgstr "&Ukloni"

#: package/contents/ui/main.qml:112
#, kde-format
msgid "Applications"
msgstr ""

#: package/contents/ui/main.qml:115
#, kde-format
msgid "Login Scripts"
msgstr ""

#: package/contents/ui/main.qml:118
#, kde-format
msgid "Pre-startup Scripts"
msgstr ""

# >> @item:intable Run application on...
#: package/contents/ui/main.qml:121
#, fuzzy, kde-format
#| msgid "Logout"
msgid "Logout Scripts"
msgstr "na odjavljivanju"

#: package/contents/ui/main.qml:130
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: package/contents/ui/main.qml:131
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr ""

#: package/contents/ui/main.qml:145
#, kde-format
msgid "Choose Login Script"
msgstr ""

#: package/contents/ui/main.qml:165
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: package/contents/ui/main.qml:182
#, kde-format
msgid "Add…"
msgstr ""

#: package/contents/ui/main.qml:197
#, kde-format
msgid "Add Application…"
msgstr ""

#: package/contents/ui/main.qml:203
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Login Script…"
msgstr "Dodaj skriptu..."

#: package/contents/ui/main.qml:209
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Logout Script…"
msgstr "Dodaj skriptu..."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Časlav Ilić"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "caslav.ilic@gmx.net"

#~ msgid "Shell script path:"
#~ msgstr "Putanja skripte školjke:"

#~ msgid "Create as symlink"
#~ msgstr "Napravi kao simvezu"

# >> @option:check
#~ msgid "Autostart only in Plasma"
#~ msgstr "Pokreni samo u Plasmi"

# >> @title:column
#~ msgid "Name"
#~ msgstr "ime"

# >> @title:column
#~ msgid "Command"
#~ msgstr "naredba"

# >> @title:column
#~ msgid "Status"
#~ msgstr "stanje"

#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "izvrši"

#~ msgid "Session Autostart Manager"
#~ msgstr "menadžer samopokretanja u sesiji"

#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr ""
#~ "Kontrolni modul menadžera samopokretanja u sesiji|/|$[svojstva dat "
#~ "'Kontrolnom modulu menadžera samopokretanja u sesiji']"

#~ msgid "Copyright © 2006–2010 Autostart Manager team"
#~ msgstr "© 2006–2010, tim menadžera samopokretanja"

#~ msgid "Stephen Leaf"
#~ msgstr "Stiven Lif"

#~ msgid "Montel Laurent"
#~ msgstr "Loren Montel"

#~ msgid "Maintainer"
#~ msgstr "održavalac"

# >> @item:intable +
#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "uključen"

# >> @item:intable +
#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "isključen"

# >? Does "Desktop file" make sense? The button is "Add Program".
# rewrite-msgid: /Desktop File/Program/
# >> @item:intable
#~ msgid "Desktop File"
#~ msgstr "programi"

# >> @item:intable
#~ msgid "Script File"
#~ msgstr "skripte"

#~ msgid "Advanced..."
#~ msgstr "Napredno..."

#~ msgid "Add Program..."
#~ msgstr "Dodaj program..."

# >> @item:intable Run application on...
#~ msgid "Startup"
#~ msgstr "po pokretanju"

# >> @item:intable Run application on...
#~ msgid "Before session startup"
#~ msgstr "pre pokretanja sesije"
